import dvc.api
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
import shutil
from typing import Dict 


def configureSim(params: Dict) -> None:
    """Plot simulation mesh as PNG file
    Args:
        params {Dir}: parameters for the stage
    """
    
    # Load params 
    params = dvc.api.params_show()
    sonicFoam_params = params['configureSim']['controlDict']['params']
    sonicFoam_endTime = sonicFoam_params['endTime']
    
    # Copy U config from last stage of sonicFoam
    try:
        shutil.copy(f"sonicFoam/{sonicFoam_endTime}/U", "scalarTransportFoam/0/U")
        print("U config copied successfully.")
    # If source and destination are same
    except shutil.SameFileError:
        print("Source and destination represents the same file.")
    # If there is any permission issue
    except PermissionError:
        print("Permission denied.")
    # For other errors
    except:
        print("Error occurred while copying file.")
    
    # Update T config
    sonicFoam_T = ParsedParameterFile(f"sonicFoam/{sonicFoam_endTime}/T")
    scalarTransportFoam_T = ParsedParameterFile("scalarTransportFoam/0/T")
    scalarTransportFoam_T.content['boundaryField'] = sonicFoam_T.content['boundaryField']    
    scalarTransportFoam_T.writeFile()
    print("T config updated successfully.")
    
    # Copy polyMesh    
    try:
        shutil.copytree("sonicFoam/constant/polyMesh", 
                        "scalarTransportFoam/constant/polyMesh", 
                        dirs_exist_ok=True)
        print("polyMesh copied successfully.")
    # If there is any permission issue
    except PermissionError:
        print("Permission denied.")
    # For other errors
    except:
        print("Error occurred while copying directory")


if __name__ == '__main__':
    
    params = dvc.api.params_show()
    configureSim(params)
