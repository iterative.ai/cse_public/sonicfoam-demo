import argparse
from collections.abc import Mapping
from dvc.repo import Repo
import git
from invoke import Context
from pathlib import Path
import re
from typing import Any, Text, List, Dict
import yaml
from yaml.loader import SafeLoader



def is_file_in_outs_dir(outs: List, file_path: Text):
    for path in outs:
        if Path(path).is_dir():
            print(path)
            # Check if the file is inside 'outs' directory 
            if path in file_path:
                return True 
            else: 
                continue 
        else:
            continue 


def replace_vars_in_outs(st_outs: List, st_out_vars: List, vars_dict: Dict): 
    """Replace templated variables in outputs path

    Args:
        st_outs (list): list of outs 
        st_out_vars (list): templated variables in st_outs 
        vars_dict (dict): variables dict 

    Returns:
        list: List of outs with replaces variables 
    """

    for i, _ in enumerate(st_outs):

        for out_var in st_out_vars:
            print(out_var)
            keys = out_var.replace('${', '').replace('}', '').split('.')
            # print(f"New keys: {keys}")

            var = vars_dict
            for key in keys:
                if var: var = var.get(key)
                
            if var:
                print(f'-> Replace {st_outs[i]}')
                st_outs[i] = st_outs[i].replace(out_var, var)
                print(f'-> New value {st_outs[i]}')
            else: 
                print('-> NO key in dict')

    print(f"\nALL OUTPUTS: {st_outs}\n")
    return st_outs


def find_dvc_stage_outs(stage_name: Text, dvc_yaml: Dict, params_yaml: Dict) -> Dict: 
    
    stage = dvc_yaml['stages'][stage_name]
    
    # Get DVC pipeline top-level vars (GLOBAL)
    p_vars = None 
    if 'vars' in dvc_yaml:
        p_vars = dvc_yaml['vars']
        p_vars_dict = {}
        {p_vars_dict.update(i) for i in p_vars}
    
    # Get list of all outs for the stage
    st_outs = []
    for outs in ['outs', 'metrics', 'plots']:
        print(outs)
        if outs in stage.keys():
            
            st_outs = stage[outs]
            print('--------------')
            print(f"Stage outputs - {outs}: {st_outs}")
            
            # Some 'outs' can be dictionaires like {path: {'cache': false}}
            # - extract only 'path' in that case
            for i, out_item in enumerate(st_outs):
                print(f"out_item: {out_item}")

                if isinstance(out_item, Mapping):
                    print(True)
                    out_file = list(out_item.keys())[0]
                    st_outs[i] = out_file

    # List all variables from the Stage config to be replaced 
    st_out_vars = []
    for out_item in st_outs:
        st_out_vars.extend(re.findall(r'\$\{.*?\}', out_item))

    # If no 'st_out_vars' -> nothing to change, return 'st_outs' as is 
    if len(st_out_vars) == 0:
        return st_outs 
    else:
        # First, replace outs with the stage vars (LOCAL)
        print('\nLOCAL')
        if 'vars' in stage.keys():
            st_vars = stage['vars']
            st_vars_dict = {}
            {st_vars_dict.update(i) for i in st_vars}

            st_outs = replace_vars_in_outs(st_outs, st_out_vars, st_vars_dict)
            print(f"UPDATED LOCAL st_outs: {st_outs}")

        # # Extract vars list for top-level vars and params
        # st_out_vars = []
        # for out_file in st_outs:
        #     st_out_vars.extend(re.findall(r'\$\{.*?\}', out_file))
        # if len(st_out_vars) == 0:
        #     return st_outs 
        print('--------------')

        # If global (top) vars used, update then (GLOBAL)
        if p_vars:
            print('\nGLOBAL')
            st_outs = replace_vars_in_outs(st_outs, st_out_vars, p_vars_dict)
            print(f"UPDATED GLOBAL st_outs: {st_outs}")

        # Finally, replace outs with vars from params.yaml (PARAMS)
        print('\nPARAMS')
        # print(params_yaml)
        st_outs = replace_vars_in_outs(st_outs, st_out_vars, params_yaml)
        print(f"UPDATED PARAMS st_outs: {st_outs}")
            
    return st_outs
                      

class DvcOutsHandler():
    
    def __init__(self, path: Text, stage_name: Text):
        self.git_repo = git.Repo('.')
        self.dvc_repo = Repo('.')
        self.stage_name = stage_name
        self.outs = None
        
        self._get_dvc_outs()
        
        
    def _get_dvc_outs(self):
        
        with open('dvc.yaml') as f:
            dvc_yaml = yaml.load(f, Loader=SafeLoader)

        with open('params.yaml') as f:
            params_yaml = yaml.load(f, Loader=SafeLoader)

        outs_list = []
        for stage_name in dvc_yaml['stages'].keys():
            outs_list.extend(find_dvc_stage_outs(stage_name, dvc_yaml, params_yaml))
        
        self.outs = outs_list

    def dvc_add(self) -> Any:
        
        dvc_files_created = []

        for file in self.git_repo.untracked_files:
            
            if file in self.outs:
                print(f"-> skip file from DVC `outs`: {file}")
                continue
            elif is_file_in_outs_dir(self.outs, file):
                print(f"-> skip file is part of DVC `outs` dir: {file}")
                continue
            elif file.endswith(('.dvc', '.gitignore', 'dvc.lock')):
                print(f"-> skip file in [.dvc', '.gitignore', 'dvc.lock")
                continue
            
            else:
                print(f"Add file to DVC: {file}")
                path = Path(f"outputs/{self.stage_name}/{file}.dvc")
                path.parent.mkdir(parents=True, exist_ok=True)
                self.dvc_repo.add(
                    targets = file,
                    recursive = False,
                    no_commit = False,
                    fname  = path,
                )
                dvc_files_created.append(str(path))
        
        return dvc_files_created

    def git_commit(self) -> Any:        
        c = Context()
        files = []

        # Select untracked files that are not DVC outs
        for file in self.git_repo.untracked_files: 
            if file not in self.outs:
                files = "  ".join([f"\"{file}\""])
        print(f"Git - Commit untracked files: {files}")
        
        if len(files) > 0:
            c.run(f"git add {files}")
            c.run(f"git commit -m 'Commit simulation changes' ")
        else: 
            print("Nothing to commit!")
        

if __name__ == '__main__':
    
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--stage', dest='stage', required=True)
    args_parser.add_argument('--out_index', dest='out_index', required=True)
    args_parser.add_argument('--commit', dest='commit', action='store_true')
    args = args_parser.parse_args()
    
    handler = DvcOutsHandler(path='.', stage_name=args.stage)
    
    print("\nDVC_OUTS_ADD: Add files to DVC version control")
    dvc_files_created = handler.dvc_add()
    
    if args.commit:
        print("DVC_OUTS: Commit the files\n")
        handler.git_commit()
        
    print("\nDVC_OUTS_ADD: Save index of dvc_files_created ")
    with open(args.out_index, "w") as output:
        output.write(str(dvc_files_created))
    
