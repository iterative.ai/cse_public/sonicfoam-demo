import dvc.api
import numpy as np
import pyvista as pv
from typing import Dict 


def plotMesh(params: Dict) -> None:
    """Plot simulation mesh as PNG file
    Args:
        params {Dir}: parameters for the stage
    """
    
    # Load params 
    params = dvc.api.params_show()
    project_dir = params['project_dir']
    post_process_dir = params['post_process_dir']
    mesh_vtk_data_file = params['plotMesh']['mesh_vtk_data_file']
    plot_mesh_file = params['plotMesh']['plot_mesh_file']
    
    # Read post-processing data 
    MESH_VTK_DATA_PATH = f"{project_dir}/{mesh_vtk_data_file}"
    print(f'Mesh VTK data file path: {MESH_VTK_DATA_PATH}')
    ## grid is the central object in VTK where every field is added on to grid
    mesh = pv.read(MESH_VTK_DATA_PATH)
    
    # Build & save plot 
    pv.start_xvfb()
    PLOT_MESH_PATH = f"{project_dir}/{post_process_dir}/{plot_mesh_file}"
    pltr = pv.Plotter(off_screen=True)
    pltr.add_mesh(mesh)
    pltr.screenshot(PLOT_MESH_PATH)
    print(f'Save mesh plot to: {PLOT_MESH_PATH}')

if __name__ == '__main__':
    
    params = dvc.api.params_show()
    plotMesh(params)
